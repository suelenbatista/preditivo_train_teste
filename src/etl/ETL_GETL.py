# TODO: delete this file.

from string import Template
from src.etl.etl_utils import *

query_getl = Template(
    """
    USE BI_DM_GETL;
    
    -- PROCEDIMENTOS DE INTERESSE
    DROP TABLE IF EXISTS #procedimentos;
    CREATE TABLE #procedimentos (idprocedimento BIGINT);
    INSERT INTO #procedimentos VALUES $procedimentos;

    ------------------------------------------------------------------------------------------------------------

    -- PROCEDIMENTOS REALIZADOS (GERAL)
    DROP TABLE IF EXISTS #sinistros_unificados;
    SELECT * INTO #sinistros_unificados FROM (
        SELECT DISTINCT idpessoa, idprocedimento, dtAtendimentoContaPaga
        FROM dbo.Fato_AgrupamentoAmbulatorio_Sigma (NOLOCK)
        WHERE idprocedimento IN (SELECT * FROM #procedimentos)
    ) AS sinistros_unificados;

    ------------------------------------------------------------------------------------------------------------

    -- IDS POSITIVOS
    DROP TABLE IF EXISTS #ids_positivos;
    SELECT DISTINCT idpessoa, MAX(dtIncioInternacao) AS dtIncioInternacao
        INTO #ids_positivos
    FROM dbo.Fato_AgrupamentoInternacao_Sigma (NOLOCK)
    WHERE $string_flag_especialidade
    GROUP BY idpessoa;

    ------------------------------------------------------------------------------------------------------------

    -- DATASET POSITIVO
    -- Buscamos ocorrências de procedimentos de interesse nos registros de todos os pacientes que tiveram desfecho positivo.
    -- Timeline da busca de procedimentos: 6 meses de procedimentos -> gap de 3 meses -> desfecho.
    -- A razão pela qual deixamos um gap entre o período de busca e a data de desfecho, é porque queremos avaliar a capacidade
    -- de detecção precoce do modelo. Além disso, queremos diminiuir a correlação do desfecho com procedimentos obrigatórios 
    -- e aumentar a aprendizagem de procedimentos indicativos de condição médica que pode levar ao desfecho.

    DROP TABLE IF EXISTS #dataset_positivo;
    SELECT #sinistros_unificados.idpessoa, #sinistros_unificados.idprocedimento
        INTO #dataset_positivo
    FROM #sinistros_unificados JOIN #ids_positivos
        ON #sinistros_unificados.IdPessoa = #ids_positivos.IdPessoa
    WHERE dtAtendimentoContaPaga < DATEADD(MONTH, -3, dtIncioInternacao) AND
            dtAtendimentoContaPaga >= DATEADD(MONTH, -9, dtIncioInternacao);

    ------------------------------------------------------------------------------------------------------------

    -- IDS NEGATIVOS (2019)
    DROP TABLE IF EXISTS #ids_negativos;
    SELECT TOP ((SELECT COUNT (DISTINCT idpessoa) FROM #ids_positivos))
        idpessoa, MAX(dtAtendimentoContaPaga) AS max_dtAtendimentoContaPaga
            INTO #ids_negativos
    FROM #sinistros_unificados
    WHERE
        YEAR(dtAtendimentoContaPaga) = 2019 AND
        IdPessoa NOT IN (SELECT idpessoa FROM #ids_positivos)
    GROUP BY idpessoa
    ORDER BY NEWID();

    ------------------------------------------------------------------------------------------------------------

    -- DATASET NEGATIVO (2019)
    DROP TABLE IF EXISTS #dataset_negativo;
    SELECT sinistros_unificados.idpessoa, sinistros_unificados.idprocedimento
        INTO #dataset_negativo
    FROM #sinistros_unificados AS sinistros_unificados JOIN #ids_negativos AS ids_negativos
        ON sinistros_unificados.IdPessoa = ids_negativos.IdPessoa
    WHERE dtAtendimentoContaPaga < max_dtAtendimentoContaPaga AND
            dtAtendimentoContaPaga >= DATEADD(MONTH, -6, max_dtAtendimentoContaPaga);
    
    ------------------------------------------------------------------------------------------------------------
    
    SELECT idpessoa, idprocedimento, 'positivo' AS dataset_type FROM #dataset_positivo
    UNION ALL
    SELECT idpessoa, idprocedimento, 'negativo' AS dataset_type FROM #dataset_negativo
    """
)

query_bariatrica_getl = query_getl.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_bariatrica),
    string_flag_especialidade='FlagBariatrica = 1'
)

query_neonatal_getl = query_getl.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_neonatal),
    string_flag_especialidade='FlagNeoNatal = 1'
)

query_bucomaxilo_getl = query_getl.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_bucomaxilo),
    string_flag_especialidade="especialidadeInt = 'Bucomaxilofacial'"
)

query_cardio_getl = query_getl.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_cardio),
    string_flag_especialidade="especialidadeInt = 'Cardiovascular'"
)

query_ortopedia_getl = query_getl.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_ortopedia),
    string_flag_especialidade="especialidadeInt = 'Ortopedia'"
)

queries_getl_by_model = {
    'bariatrica': query_bariatrica_getl,
    'neonatal': query_neonatal_getl,
    'ortopedia': query_ortopedia_getl,
    'bucomaxilo': query_bucomaxilo_getl,
    'cardio': query_cardio_getl
}

if __name__ == '__main__':
    print(queries_getl_by_model.values())
